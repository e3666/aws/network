# Network

## VPC
Uma VPC (virtual private cloud) é uma rede isolada dentro da AWS, sendo que essa rede possui um range de IPs e toda VPC é isolada de outras VPCs, mesmo elas sendo da mesma conta, na mesma região.

Dentro de uma VPC você pode criar recursos como subnets, security groups e configurar route tables.

Uma VPC é um serviço a nível de região. Ou seja, os recursos de multiplas AZs podem residir dentro de uma mesma VPC.

<img src='img/fig-02.svg'/>

> OBS.: Não à nenhum custo em se ter uma VPC

## Subnets
Uma subnet é um range de IPs da VPC, e é dentro da subnet que vão residir os serviços da aws, como por exemplo, EC2. A subnet pode ser publica ou privada, sendo assim você define se uma aplicação deverá ficar na subnet publica ou privada de acordo com a finalidade dela. Em uma subnet é possível colocar diversas camadas de proteção, como network acces control list (ACL) e security groups (que são a nível de instância).

Diferentemente da VPC, uma subnet não é a nível de região. Logo você deve criar subnets conforme a necessidade para cada AZ

<img src='img/fig-03.svg'/>

## Route Tables
É um conjunto de regras, que define para onde o trafego de rede da VPC irá. Você pode associar uma subnet a uma tabela específica, mas ela é implicitamente associada a main route table.

Cada rota na tabela especifica um range de IPs para onde o tráfego irá e o gateway, network interface ou conexão que o tráfego de rede utilizará. Por exemplo, tudo que trafegar com o destino no ip 172.31.0.0/16 se refere ao target local, ou seja, a própria VPC. Já se o destino for 0.0.0.0/0 então o target é o internet gateway (que dará acesso a internet pública)

## Internet Gateway
Para permitir a conexão entre a internet pública e a VPC é preciso de um Internet Gateway. Que basicamente funciona como uma porta para dentro da VPC.

<img src='img/fig-04.svg'/>

## NAT gateway
Com o Internet Gateway seus recursos da subnet pública ganharam acessó a internet, porém os recuros dentro da subnet privada não. Então para isso temos o NAT gateway, que é um recurso que reside dentro da subnet publica, e que funciona de forma unidirecional, sendo ele um ponto de acesso para a subnet privada acessar e requisitar algo da internet.

<img src='img/fig-05.svg'/>

## Virtual Private Gateway
Assim como o Internet Gateway, esse gateway também é um modo de criar uma porta para com a internet, porém diferentemente do Internet Gateway que permite a comunicação com a internet como um todo, essa porta permite somente conexões de uma rede aprovada, como por exemplo uma VPN.

## AWS Direct Connect
Mesmo o Virtual Private Gateway você utilizada da internet comum para se comunicar com a vpc, a diferença é que agora só você consegue acessar. Porém com esse serviço, você tem uma conexão direta entre seu data center e a sua cloud. Literalmente a aws conecta via cabo as duas coisas. E todo o trafego acontece pela rede da aws, sendo extremamente mais seguro, e mais rápido. Mas para isso você precisa entrar em contato com um parceiro proximo para que a conexão seja feita.




###############################################
Nota:
Caso necessite de um ip público, você pode utilizar duas formas de atribuição.
- Auto-assign: A aws define o ip da máquina, mas esse ip pode mudar durante eventos de instância, como um reboot
- Elastic IP address: é um serviço da aws, em que você define um ip e o atribui a uma máquina, e esse ip é atrelado a conta, logo ele não muda nunca.
###############################################
